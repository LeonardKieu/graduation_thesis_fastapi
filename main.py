import io
from typing import Optional

import cv2
import numpy as np
from PIL import Image
from PIL.JpegImagePlugin import JpegImageFile
from fastapi import File, UploadFile

from config.base import TESSERACT_CONFIG, app
from service.ner import NERPerformer
from service.ocr import OCRPerformer1
from service.preprocess import NewPreprocessor
from service.retrieval import MyDocument, Seeker


@app.get("/health")
def check_health():
    return {"status": "Still Alive ahihi!"}


@app.get("/retrieve")
def retrieve(text: str):
    res = Seeker(text=text).perform()

    return {
        "status": "OK",
        "message": "Ahihi",
        "data": res
    }


@app.post("/kie")
async def perform_kie(img: UploadFile = File(...), language: Optional[str] = 'vie') -> dict:
    # Receive img, lang
    bytes_img: bytes = await img.read()
    pil_image: JpegImageFile = Image.open(io.BytesIO(bytes_img))
    np_img: np.ndarray = cv2.cvtColor(np.asarray(pil_image), cv2.COLOR_RGB2BGR)

    # Preprocess to enhance img quality
    preprocessor = NewPreprocessor(
        raw_img=np_img, language=language,
        # show_img=True
    )
    clean_img = preprocessor.preprocess()

    # OCR
    ocr_performer = OCRPerformer1(
        # show_img=True
    )
    bboxes = ocr_performer.detect_text(clean_img)
    extracted_text, drawn_img = ocr_performer.recognize_text(bboxes, clean_img)

    # # Information extraction
    # ner_performer = NERPerformer()
    # ner_performer.perform(extracted_txt)
    # ner_performer.show_result()

    # Save unstructured doc into Elasticsearch
    doc = MyDocument(extracted_text=extracted_text)
    doc.upload_img_into_s3(img=drawn_img, filename=img.filename)
    doc.save()

    # Return to FE
    return {
        "status": "OK",
        "message": "Perform OCR successfully!",
        "data": doc.to_dict()
    }
