from elasticsearch_dsl import Document, Keyword, Text, analyzer, tokenizer
from elasticsearch_dsl import Q, MultiSearch, Search
from elasticsearch_dsl.query import MultiMatch
from typing import List, Optional
from elasticsearch_dsl.response import Response
from config.base import elasticsearch_index_name, bucket_name
import boto3

client = boto3.client('s3', region_name='ap-southeast-1')

my_analyzer = analyzer(
    'my_analyzer',
    tokenizer=tokenizer('standard'),
    filter=['lowercase']
)


class MyDocument(Document):
    img_url = Keyword()
    extracted_text = Text(analyzer=my_analyzer)

    class Index:
        name = elasticsearch_index_name
        analyzers = [
            my_analyzer
        ]

    def upload_img_into_s3(self, img, filename: str):
        key = f'imgs/{filename}'
        upload_res = client.put_object(Body=img, Bucket=bucket_name, Key=key, ContentType='image/jpeg', ACL='public-read')
        self.img_url = f"https://{bucket_name}.s3-ap-southeast-1.amazonaws.com/{key}"


class Seeker:

    def __init__(self, text):
        self._text = text

    def _build_and_run_query(self) -> Response:
        s = Search(index=elasticsearch_index_name)
        s.query = MultiMatch(query=self._text, fields=['extracted_text'])
        return s.execute()

    def _preprocess(self, response: Response) -> List[dict]:
        return [hit.to_dict() for hit in response]

    def perform(self):
        res = self._build_and_run_query()
        return self._preprocess(res)
